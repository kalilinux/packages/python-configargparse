Source: python-configargparse
Maintainer: Raphaël Hertzog <hertzog@debian.org>
Section: python
Priority: optional
Build-Depends: python-setuptools (>= 0.6b3), python3-setuptools, python-all (>= 2.6.6-3), python3-all, debhelper (>= 9), dh-python
Standards-Version: 3.9.6
Homepage: https://github.com/zorro3/ConfigArgParse
Vcs-Git: git://git.kali.org/packages/python-configargparse.git
Vcs-Browser: http://git.kali.org/gitweb/?p=packages/python-configargparse.git;a=summary

Package: python-configargparse
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Description: Enhanced version of the Python 'argparse' library
 Applications with more than a handful of user-settable options are best
 configured through a combination of command line args, config files,
 hard-coded defaults, and in some cases, environment variables.
 .
 Python's command line parsing modules such as argparse have very limited
 support for config files and environment variables, so this module
 extends argparse to add these features.
 .
 This package provides the library for Python 2.x.

Package: python3-configargparse
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Description: Enhanced version of the Python 'argparse' library (Python 3)
 Applications with more than a handful of user-settable options are best
 configured through a combination of command line args, config files,
 hard-coded defaults, and in some cases, environment variables.
 .
 Python's command line parsing modules such as argparse have very limited
 support for config files and environment variables, so this module
 extends argparse to add these features.
 .
 This package provides the library for Python 3.x.
